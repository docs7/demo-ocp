# Red Hat OpenShift Container Platform

![banner](docs/images/OpenShiftContainerPlatform.png)

* [Projects (Tenant) Management](docs/01-PROJECT.md)
* [Application Deployment](docs/02-APPLICATION_DEPLOYMENT.md)
* [Deployment Strategy](docs/03-DEPLOYMENT_STRATEGY.md)
* [Horizontal Pod Autoscaler (HPA)](docs/04-AUTOSCALE.md)
* [Traffic Control Security](docs/05-TRAFFIC_CONTROL.md)
* [Log & Metrics and Monitoring](docs/06-MONITORING.md)
* [Service Mesh](docs/07-SERVICE_MESH.md)

